import { ICoach, IDirection, IDiscipline, IElementDirectionType, ITraining } from "../types/types"


export type ITrainingsWithDate = {
    [key: string]: Array<ITraining>
}
export function trainingsWithDate(trainingsArray: Array<ITraining>): ITrainingsWithDate {
    const sortTrainings = trainingsArray.sort((a, b) => {
        return a.start - b.start
    })

    const groupTimeTrainings = {} as ITrainingsWithDate

    sortTrainings.forEach(training => {
        const [, hours, day, month, year] = parseDate(training.start)
        const dateString = `${hours}-${day}-${month}-${year}`

        if (groupTimeTrainings.hasOwnProperty(dateString)) {
            groupTimeTrainings[dateString].push(training)
        } else {
            groupTimeTrainings[dateString] = [training]
        }
    })
    
    return groupTimeTrainings
}

export function parseDate(timeStamp: number) {
    const date = new Date(timeStamp)
    let year = date.getFullYear() + ''
    let month = date.getMonth() + 1 + ''
    let day = date.getDate() + ''
    let hours = date.getHours() + ''
    let minutes = date.getMinutes() + ''

    if (month.length === 1) {
        month = '0' + month
    }
    if (day.length === 1) {
        day = '0' + day
    }            
    if (hours.length === 1) {
        hours = '0' + hours
    }
    if (minutes.length === 1) {
        minutes = '0' + minutes
    }

    return [minutes, hours, day, month, year]
}

export function sortTrainingsById(training: Array<ITraining>) {
    return [...training].sort((a, b) => a.id - b.id)
}

export function sortCoachesBySurname(coaches: Array<ICoach>) {
    return [...coaches].sort((a, b) => a.surname.localeCompare(b.surname))
}

export function sortCoachesById(coaches: Array<ICoach>) {
    return [...coaches].sort((a, b) => a.id - b.id)
}

export function sortDisciplinesByTitle(disciplines: Array<IDiscipline>) {
    return [...disciplines].sort((a, b) => a.title.localeCompare(b.title))
}

export function sortDisciplinesById(disciplines: Array<IDiscipline>) {
    return [...disciplines].sort((a, b) => a.id - b.id)
}

export function sortDirectionsByTitle(directions: Array<IDirection>) {
    return [...directions].sort((a, b) => a.title.localeCompare(b.title))
}

export function sortDirectionsById(directions: Array<IDirection>) {
    return [...directions].sort((a, b) => a.id - b.id)
}

export function getCheckedIds(elems: IElementDirectionType, prefix: string): Array<number> {
    const includes: Array<number> = []

    for (let key in elems) {
        if (elems.hasOwnProperty(key)) {
            const isChecked = elems[key].checked           
            if (key.startsWith(prefix) && isChecked) {
                const disciplineId = +key.replace(prefix, '')
                includes.push(disciplineId)
            }
        }
    }
    const includesSort = includes.sort((a, b) => a - b)
    return includesSort
}

export function lockBodyElement() {
    document.body.classList.add('lock')
}

export function unlockBodyElement() {
    document.body.classList.remove('lock')
}