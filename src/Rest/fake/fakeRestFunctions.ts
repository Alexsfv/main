import { ICoach, IDirection, IDiscipline, IGetAdminStatus, IGetToken, ITraining } from "../../types/types"

const access_token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1OTIyODA3OTIsInVzZXJfbmFtZSI6ImFkbWluIiwiYXV0aG9yaXRpZXMiOlsiYWRtaW4iXSwianRpIjoiOTU0MDdkMmUtNTg0ZC00MjAwLThlNDUtNTIyODljM2JmYmY3IiwiY2xpZW50X2lkIjoidmlydHVhbF9jbHViIiwic2NvcGUiOlsiZm9vIiwicmVhZCIsIndyaXRlIl19.b6Gq-DruY2PgjEk0P6yDj-nBX6H2HEzVxc71x1ReF5uyEkQUYHdet7SmdjwovMAPDrV3-R-LPTbKpA3xTkRD5HdFwhkWRv3QE_CqUwEvBD0aybzD2-1Zp7FiRuToVB-oBTM81oP4lRHz6KH7qDpPd0NogMLV2fhVWUcm_fcWKUNlp0JvWJE2RZxWYcMC4IWsMktgNIbkZ_d-XTh1FrozLAi9jYL8L4dbckcR4GxaofwoXDM7UquC6QIrR_0Qx4K7AN_TZebYjZHRxCJjvZGUMLJ9EHBA2givHapWJqKFk14xI7aoyLg70-yejNEk6-AJZamhbgXEY4JcA_enwkMp4w"

export const trainingTemplate: ITraining = {
  "id": 17,
  "start": 1592802000520,
  "finish": 1592805300279,
  "title": "ABS+STRETCHING",
  "description": "ABS+STRETCHING - тренировка направлена на проработку мышц брюшного пресса, спины и на развитие гибкости с использованием специальных упражнений на растягивание, увеличения подвижности суставов. Рекомендуется для вех уровней подготовленности.",
  "category": null,
  "store": {
  "id": 1
  },
  "discipline": {
  "id": 1
  },

  "coach": {
    "id": 1,
    "deleted": false,
    "userId": null,
    "surname": "Постникова",
    "firstName": "Анастасия",
    "patronymic": null,
    "fullName": "Постникова Анастасия ",
    "defaultStoreId": null,
    "stores": null,
    "role": null,
    "phone": null,
    "email": null,
    "sex": null,
    "description": null
  },

  "clients": [
  {
      "id": 126,
      "deleted": false,
      "userId": 126,
      "surname": null,
      "firstName": "Наталья",
      "patronymic": null,
      "fullName": "Наталья",
      "defaultStoreId": 1,
      "stores": [
      1
      ],
      "role": null,
      "phone": "79655026085",
      "email": "natikisaeva@mail.ru",
      "sex": null,
      "city": "Екатеринбург"
  },
  null,
  null,
  null,
  null,
  null,
  null,
  null
  ],

  "completed": false,
  "deleted": false,
  "duration": 55.0,
  "eventCalendarPlanning": null,
  "debitedClientService": {
  "id": null
  },

  "free": true,
  "numberMinutesBeforeClosing": 15,
  "placesLimit": 15,
  "canceled": false,
  "confirmed": false,
  "clientConfirmed": false
}

/// FAKE REST FUNCTION!
export const getTokenData = async (login: string, password: string): Promise<IGetToken> => {
    
    let data = {"access_token": "I7aoyLg70-ykMp4w",
    "token_type": "bearer",
    "refresh_token": "--8967896789",
    "expires_in": 86399,
    "scope": "foo read write",
    "jti": "9540700-8e45-52289c3bfbf7"}

    if (login === '1s2s3' && password === '1s2s3') {
        data = {
            "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1OTIyODA3OTIsInVzZXJfbmFtZSI6ImFkbWluIiwiYXV0aG9yaXRpZXMiOlsiYWRtaW4iXSwianRpIjoiOTU0MDdkMmUtNTg0ZC00MjAwLThlNDUtNTIyODljM2JmYmY3IiwiY2xpZW50X2lkIjoidmlydHVhbF9jbHViIiwic2NvcGUiOlsiZm9vIiwicmVhZCIsIndyaXRlIl19.b6Gq-DruY2PgjEk0P6yDj-nBX6H2HEzVxc71x1ReF5uyEkQUYHdet7SmdjwovMAPDrV3-R-LPTbKpA3xTkRD5HdFwhkWRv3QE_CqUwEvBD0aybzD2-1Zp7FiRuToVB-oBTM81oP4lRHz6KH7qDpPd0NogMLV2fhVWUcm_fcWKUNlp0JvWJE2RZxWYcMC4IWsMktgNIbkZ_d-XTh1FrozLAi9jYL8L4dbckcR4GxaofwoXDM7UquC6QIrR_0Qx4K7AN_TZebYjZHRxCJjvZGUMLJ9EHBA2givHapWJqKFk14xI7aoyLg70-yejNEk6-AJZamhbgXEY4JcA_enwkMp4w",
            "token_type": "bearer",
            "refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsImF1dGhvcml0aWVzIjpbImFkbWluIl0sImp0aSI6IjMyNDIwNmZjLTc1ZmQtNDYyYS1hZDZmLWFmYTdjNDBlMWEyMSIsImNsaWVudF9pZCI6InZpcnR1YWxfY2x1YiIsInNjb3BlIjpbImZvbyIsInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiI5NTQwN2QyZS01ODRkLTQyMDAtOGU0NS01MjI4OWMzYmZiZjcifQ.PLywFf1Lg0zrqjfIr3n--7xgYbJMbckOp9KlRoTcJwKIkNkEYv6xK-qzUGH3YpDxb4NLcwnNBVsOrr_MY7uUjTSbQwY63nb66Wp5f176jfg40TUMfz_oNOxEiODYoNsdnIhZoU4ApHkPSP8IqaUsDHKhdToOlOEPfeee-ujDf589oa-5QWQY9fGsUa6q_Tg6D0KwFwAvT6jj_EGajHC7G1RHwCEefD0WzsEvEuevqfYUPUeADNNvviq_-4BNhbC1tFkW-1pbf-RGhc-g1e_wnbvVS7oQ_ybq9cCKMA44vHLX_tQKkalx2hK9cQxbIWAN5Da7Lwh5Y5JEcJQVQwf4Zw",
            "expires_in": 86399,
            "scope": "foo read write",
            "jti": "95407d2e-584d-4200-8e45-52289c3bfbf7"
        }
    }
    
    return data
}

/// FAKE REST FUNCTION!
export const getAdminStatus = async (token : string): Promise<boolean> => {
    let response: IGetAdminStatus = {
        "is_admin": false
    }
    
    if (token === access_token) {
        response = {
            "is_admin": true
        }
    }
    const isAdmin = response.is_admin
    return isAdmin
}

/// FAKE REST FUNCTION!
export const getTrainings = async (token: string | null): Promise<Array<ITraining>> => {
    const trainings = [
        {
            "id": 3,
            "start": 159250000000,
            "finish": 1592599999999,
            "title": "ABS+7777",
            "description": "ABS+STRETCHING - тренировка направлена на проработку мышц брюшного пресса, спины и на развитие гибкости с использованием специальных упражнений на растягивание, увеличения подвижности суставов. Рекомендуется для вех уровней подготовленности.",
            "category": null,
            "store": {
              "id": 1
            },
            "discipline": {
              "id": 1
            },
            "coach": {
              "id": 1,
              "deleted": false,
              "userId": null,
              "surname": "Дмитриева",
              "firstName": "Наталья",
              "patronymic": null,
              "fullName": "Дмитриева Наталья ",
              "defaultStoreId": null,
              "stores": null,
              "role": null,
              "phone": null,
              "email": null,
              "sex": null,
              "description": null
            },
            "clients": [
              {
                "id": 126,
                "deleted": false,
                "userId": 126,
                "surname": null,
                "firstName": "Наталья",
                "patronymic": null,
                "fullName": "Наталья",
                "defaultStoreId": 1,
                "stores": [
                  1
                ],
                "role": null,
                "phone": "79655026085",
                "email": "natikisaeva@mail.ru",
                "sex": null,
                "city": "Екатеринбург"
              },
              null,
              null,
              null,
              null,
              null,
              null,
              null
            ],
            "completed": false,
            "deleted": false,
            "duration": 55.0,
            "eventCalendarPlanning": null,
            "debitedClientService": {
              "id": null
            },
            "free": true,
            "numberMinutesBeforeClosing": 0,
            "placesLimit": 35,
            "canceled": false,
            "confirmed": false,
            "clientConfirmed": false
          },
          {
            "id": 14,
            "start": 1592520145201,
            "finish": 1592873323702,
            "title": "ABS+3333",
            "description": "ABS+STRETCHING - тренировка направлена на проработку мышц брюшного пресса, спины и на развитие гибкости с использованием специальных упражнений на растягивание, увеличения подвижности суставов. Рекомендуется для вех уровней подготовленности.",
            "category": null,
            "store": {
              "id": 1
            },
            "discipline": {
              "id": 1
            },
            "coach": {
              "id": 1,
              "deleted": false,
              "userId": null,
              "surname": "Постникова",
              "firstName": "Анастасия",
              "patronymic": null,
              "fullName": "Постникова Анастасия ",
              "defaultStoreId": null,
              "stores": null,
              "role": null,
              "phone": null,
              "email": null,
              "sex": null,
              "description": null
            },
            "clients": [
              {
                "id": 126,
                "deleted": false,
                "userId": 126,
                "surname": null,
                "firstName": "Наталья",
                "patronymic": null,
                "fullName": "Наталья",
                "defaultStoreId": 1,
                "stores": [
                  1
                ],
                "role": null,
                "phone": "79655026085",
                "email": "natikisaeva@mail.ru",
                "sex": null,
                "city": "Екатеринбург"
              },
              null,
              null,
              null,
              null,
              null,
              null,
              null
            ],
            "completed": false,
            "deleted": false,
            "duration": 55.0,
            "eventCalendarPlanning": null,
            "debitedClientService": {
              "id": null
            },
            "free": true,
            "numberMinutesBeforeClosing": 0,
            "placesLimit": 15,
            "canceled": false,
            "confirmed": false,
            "clientConfirmed": false
          },
          {
            "id": 16,
            "start": 1592228145201,
            "finish": 1592993323702,
            "title": "Гимнастика",
            "description": "ABS+STRETCHING - тренировка направлена на проработку мышц брюшного пресса, спины и на развитие гибкости с использованием специальных упражнений на растягивание, увеличения подвижности суставов. Рекомендуется для вех уровней подготовленности.",
            "category": null,
            "store": {
              "id": 1
            },
            "discipline": {
              "id": 1
            },
            "coach": {
              "id": 1,
              "deleted": false,
              "userId": null,
              "surname": "Постникова",
              "firstName": "Анастасия",
              "patronymic": null,
              "fullName": "Постникова Анастасия ",
              "defaultStoreId": null,
              "stores": null,
              "role": null,
              "phone": null,
              "email": null,
              "sex": null,
              "description": null
            },
            "clients": [
              {
                "id": 126,
                "deleted": false,
                "userId": 126,
                "surname": null,
                "firstName": "Наталья",
                "patronymic": null,
                "fullName": "Наталья",
                "defaultStoreId": 1,
                "stores": [
                  1
                ],
                "role": null,
                "phone": "79655026085",
                "email": "natikisaeva@mail.ru",
                "sex": null,
                "city": "Екатеринбург"
              },
              null,
              null,
              null,
              null,
              null,
              null,
              null
            ],
            "completed": false,
            "deleted": false,
            "duration": 55.0,
            "eventCalendarPlanning": null,
            "debitedClientService": {
              "id": null
            },
            "free": true,
            "numberMinutesBeforeClosing": 20,
            "placesLimit": 0,
            "canceled": false,
            "confirmed": false,
            "clientConfirmed": false
          }
    ]
    return trainings
}

export const getCoaches = async (token: string | null): Promise<Array<ICoach>> => {
  const coaches: Array<ICoach> = [
    {
      "id": 2,
      "name": null,
      "deleted": false,
      "userId": null,
      "surname": "Овчинникова",
      "firstName": "Анастасия",
      "patronymic": null,
      "fullName": "Овчинникова Анастасия",
      "defaultStoreId": null,
      "stores": null,
      "role": null,
      "phone": null,
      "email": null,
      "sex": null,
      "description": null
    },
    {
      "id": 1,
      "name": null,
      "deleted": false,
      "userId": null,
      "surname": "Постникова",
      "firstName": "Анастасия",
      "patronymic": null,
      "fullName": "Постникова Анастасия ",
      "defaultStoreId": null,
      "stores": null,
      "role": null,
      "phone": null,
      "email": null,
      "sex": null,
      "description": null
    },
    {
      "id": 3,
      "name": null,
      "deleted": false,
      "userId": null,
      "surname": "Карганская",
      "firstName": "Юлия",
      "patronymic": null,
      "fullName": "Карганская Юлия ",
      "defaultStoreId": null,
      "stores": null,
      "role": null,
      "phone": null,
      "email": null,
      "sex": null,
      "description": null
    },
    {
      "id": 4,
      "name": null,
      "deleted": false,
      "userId": null,
      "surname": "Лебедева",
      "firstName": "Ольга",
      "patronymic": null,
      "fullName": "Лебедева Ольга",
      "defaultStoreId": null,
      "stores": null,
      "role": null,
      "phone": null,
      "email": null,
      "sex": null,
      "description": null
    },
    {
      "id": 5,
      "name": null,
      "deleted": false,
      "userId": null,
      "surname": "Шамаева",
      "firstName": "Алина",
      "patronymic": null,
      "fullName": "Шамаева Алина",
      "defaultStoreId": null,
      "stores": null,
      "role": null,
      "phone": null,
      "email": null,
      "sex": null,
      "description": null
    },
    {
      "id": 6,
      "name": null,
      "deleted": false,
      "userId": null,
      "surname": "Бекаури",
      "firstName": "Майя",
      "patronymic": null,
      "fullName": "Бекаури Майя",
      "defaultStoreId": null,
      "stores": null,
      "role": null,
      "phone": null,
      "email": null,
      "sex": null,
      "description": null
    },
    {
      "id": 7,
      "name": null,
      "deleted": false,
      "userId": null,
      "surname": "Чурбанова",
      "firstName": "Анриета",
      "patronymic": null,
      "fullName": "Чурбанова Анриета",
      "defaultStoreId": null,
      "stores": null,
      "role": null,
      "phone": null,
      "email": null,
      "sex": null,
      "description": null
    }
  ]
  return coaches
}

export const getDisciplines = async (token: string | null): Promise<Array<IDiscipline>> => {
  const disciplines = [
    {
      "id" : 2,
      "title" : "Дисциплина1",
      "includes" : [1, 2] //Айдишники тренеров
    },
    {
      "id" : 1,
      "title" : "Гимнастика2",
      "includes" : [3, 4] //Айдишники тренеров
    },
    {
      "id" : 3,
      "title" : "Дисциплина3",
      "includes" : [1, 2, 3] //Айдишники тренеров
    },
    {
      "id" : 5,
      "title" : "Дисциплина4",
      "includes" : [1, 2, 5] //Айдишники тренеров
    },
    {
      "id" : 4,
      "title" : "Дисциплина5",
      "includes" : [5, 7] //Айдишники тренеров
    },
    {
      "id" : 7,
      "title" : "Дисциплина6",
      "includes" : [3, 1] //Айдишники тренеров
    },
    {
      "id" : 6,
      "title" : "Дисциплина7",
      "includes" : [1, 2, 3, 5, 6, 7] //Айдишники тренеров
    }
  ]
  return disciplines
}

export const getDirections = async (token: string | null): Promise<Array<IDirection>> => {
  const directions = [
    {
      "id" : 2,
      "title" : "Направление 1",
      "includes" : [1, 2] //Айдишники программ (disciplines)
    },
    {
      "id" : 1,
      "title" : "Направление 2",
      "includes" : [3, 4]
    },
    {
      "id" : 3,
      "title" : "Направление 3",
      "includes" : [1, 2, 3]
    },
    {
      "id" : 5,
      "title" : "Направление 4",
      "includes" : [1, 2, 5]
    },
    {
      "id" : 4,
      "title" : "Направление 5",
      "includes" : [5, 2]
    },
    {
      "id" : 7,
      "title" : "Направление 6",
      "includes" : [3, 4]
    },
    {
      "id" : 6,
      "title" : "Направление 7",
      "includes" : [1, 2, 3, 5, 4]
    }
  ]
  return directions
}