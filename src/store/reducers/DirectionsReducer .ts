import { IDirection } from "../../types/types"
import { DIRECTION_UPDATE_ALL } from "../actions/ActionTypes"
import { IAllDirectionsActions } from "../actions/DirectionsActions"

export type IDirectionsState = {
    directions: Array<IDirection>
}

const directionsState: IDirectionsState = {
    directions: []
}

export default function DirectionsReducer(state = directionsState, action: IAllDirectionsActions) {
    switch(action.type) {
        case(DIRECTION_UPDATE_ALL): {
            return {
                ...state,
                directions: [...action.payload]
            }
        }
        default:
            return state
    }
}