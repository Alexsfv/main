import { AUTH_SET_ERROR_MESSAGE_FORM, AUTH_SET_LOGIN_MESSAGE, AUTH_SET_PASSWORD_MESSAGE, AUTH_SET_VALID_FORM } from '../actions/ActionTypes'
import { IAllAuthFormAtctions } from '../actions/AuthFormActions'


export type IStateAuthForm = {
    isValidForm: boolean
    loginErrorMessage: string
    passwordErrorMessage: string
    formErrorMessage: string
}
const initialState: IStateAuthForm = {
    isValidForm: false,
    loginErrorMessage: '',
    passwordErrorMessage: '',
    formErrorMessage: ''
}

export function AuthFormReducer (state = initialState, action: IAllAuthFormAtctions): IStateAuthForm {
    switch (action.type) {
        case (AUTH_SET_LOGIN_MESSAGE): {
            return {
                ...state,
                isValidForm: false,
                loginErrorMessage: action.payload
            }
        }
        case (AUTH_SET_PASSWORD_MESSAGE): {
            return {
                ...state,
                isValidForm: false,
                passwordErrorMessage: action.payload
            }
        }
        case (AUTH_SET_VALID_FORM): {
            return {
                ...state,
                isValidForm: true,
                loginErrorMessage: '',
                passwordErrorMessage: '',
                formErrorMessage: ''
            }
        }
        case (AUTH_SET_ERROR_MESSAGE_FORM): {
            return {
                ...state,
                formErrorMessage: action.payload
            }
        }
        default: {
            return {...state}
        }
    }
}
