import { APP_CHANGE_ADMIN_STATUS } from "../actions/ActionTypes"
import { AllAppActions } from "../actions/AppActions"


export type IStateApp = {
    loader: boolean
    isAdmin: boolean | null
}

const initialState: IStateApp = {
    loader: true,
    isAdmin: null,

}

export function AppReducer(state = initialState, action: AllAppActions): IStateApp {
    switch (action.type) {
        case(APP_CHANGE_ADMIN_STATUS): {
            return {
                ...state,
                isAdmin: action.payload
            }
        }

        default:
            return state
    }
}