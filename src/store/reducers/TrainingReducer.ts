import { ITrainingsWithDate } from "../../Rest/convertFunctions"
import { ITraining } from "../../types/types"
import { TRAINING_UPDATE_TRAININGS } from "../actions/ActionTypes"
import { AllTrainigActions } from "../actions/TrainingActions"

export type ITrainingState = {
    isLoading: boolean
    trainings: Array<ITraining> | []
    trainingsWithDate: ITrainingsWithDate
    dateKeys: Array<string>
}
const TrainingState: ITrainingState = {
    isLoading: true,
    trainings: [],
    trainingsWithDate: {},
    dateKeys: []
}

export function TrainingReducer(state = TrainingState, action: AllTrainigActions): ITrainingState {
    switch(action.type) {
        case(TRAINING_UPDATE_TRAININGS): {
            return {
                ...state,
                trainings: [...action.payload.newTrainings],
                trainingsWithDate: {...action.payload.trainingsDateObj},
                dateKeys: [...action.payload.trainingsDateKeys],
                isLoading: false
            }
        }
        default: 
            return state
    }
}
