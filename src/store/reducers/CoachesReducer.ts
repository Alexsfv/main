import { ICoach } from "../../types/types"
import { COACHES_UPDATE_ALL } from "../actions/ActionTypes"

import { IAllCoachActions } from '../actions/CoachesActions'

export type ICoachesState = {
    coaches: Array<ICoach>
}

const coachesState: ICoachesState = {
    coaches: []
}

export default function CoachesReducer(state = coachesState, action: IAllCoachActions) {
    switch(action.type) {
        case(COACHES_UPDATE_ALL): {
            return {
                ...state,
                coaches: [...action.payload]
            }
        }
        default:
            return state
    }
}