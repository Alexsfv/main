import { IDiscipline } from "../../types/types"
import { DISCIPLINES_UPDATE_ALL } from "../actions/ActionTypes"
import { IAllDisciplineshActions } from "../actions/DisciplinesActions"

export type IDisciplinesState = {
    disciplines: Array<IDiscipline>
}

const disciplinesState: IDisciplinesState = {
    disciplines: []
}

export default function DisciplinesReducer(state = disciplinesState, action: IAllDisciplineshActions) {
    switch(action.type) {
        case(DISCIPLINES_UPDATE_ALL): {
            return {
                ...state,
                disciplines: [...action.payload]
            }
        }
        default:
            return state
    }
}