import { combineReducers } from 'redux'
import { AppReducer } from './AppReducer'
import { AuthFormReducer } from './AuthFormReducer'
import CoachesReducer from './CoachesReducer'
import DirectionsReducer from './DirectionsReducer '
import DisciplinesReducer from './DisciplinesReducer'
import { TrainingReducer } from './TrainingReducer'

export const rootReducer = combineReducers({
    authForm: AuthFormReducer,
    app: AppReducer,
    trainings: TrainingReducer,
    coaches: CoachesReducer,
    disciplines: DisciplinesReducer,
    directions: DirectionsReducer
})

export type IRootState = ReturnType<typeof rootReducer>