import { Dispatch } from "react"
import { getCookie } from "../../cookie/cookie"
import { getCoaches } from "../../Rest/fake/fakeRestFunctions"
import { ICoach, IElementCoachType } from "../../types/types"
import { COACHES_UPDATE_ALL } from "./ActionTypes"
import { IRootState } from "../reducers/rootReducer"
import { sortCoachesById, sortCoachesBySurname } from "../../Rest/convertFunctions"


export const updateCoaches = () => {
    return async (dispatch: Dispatch<IAllCoachActions>) => {
        try {
            const access_token = getCookie('_access_user_token_bf')
            const coaches = await getCoaches(access_token)
            const sortedCoaches = sortCoachesBySurname(coaches)
            dispatch(updateCoachesCreator(sortedCoaches)) 
        } catch(e) {
            console.log(e)
        }
    }
}

export const addCoach = (elems: IElementCoachType) => {
    return async (dispatch: Dispatch<IAllCoachActions>, getState: () => IRootState) => {
        try {
            const coaches = [...getState().coaches.coaches]
            const sortedCoachesById = sortCoachesById(coaches)
            const lastId = sortedCoachesById[sortedCoachesById.length - 1].id
            
            const coach = {
                id: lastId + 1,
                name: null,
                deleted: false,
                userId: null,
                surname: elems.secondName.value,
                firstName: elems.firstName.value,
                patronymic: null,
                fullName: `${elems.secondName.value} ${elems.firstName.value}`,
                defaultStoreId: null,
                stores: null,
                role: null,
                phone: elems.phone.value,
                email: elems.email.value,
                sex: elems.sex.value,
                description: elems.description.value
            }
            
            coaches.push(coach)
            const sortedCoachesBySurname = sortCoachesBySurname(coaches)
    
            dispatch(updateCoachesCreator(sortedCoachesBySurname)) 
        } catch(e) {
            console.log(e)
        } 
    }
}

export const editCoach = (elems: IElementCoachType, coachEdit: ICoach) => {
    return async (dispatch: Dispatch<IAllCoachActions>, getState: () => IRootState) => {
        try {
            const coaches = [...getState().coaches.coaches]
            const withoutOldCoach = coaches.filter(coach => coach.id !== coachEdit.id)
    
            const coach = {
                id: coachEdit.id,
                name: coachEdit.name,
                deleted: coachEdit.deleted,
                userId: coachEdit.userId,
                surname: elems.secondName.value,
                firstName: elems.firstName.value,
                patronymic: coachEdit.patronymic,
                fullName: `${elems.secondName.value} ${elems.firstName.value}`,
                defaultStoreId: coachEdit.defaultStoreId,
                stores: coachEdit.stores,
                role: coachEdit.role,
                phone: elems.phone.value,
                email: elems.email.value,
                sex: elems.sex.value,
                description: elems.description.value
            }
            const newCoaches = withoutOldCoach.concat(coach)
            const sortedCoachesBySurname = sortCoachesBySurname(newCoaches)
            dispatch(updateCoachesCreator(sortedCoachesBySurname)) 
        } catch(e) {
            console.log(e)
        }
    }
}

type IUpdateCoachesCreator = {
    type: typeof COACHES_UPDATE_ALL
    payload: Array<ICoach>
}
const updateCoachesCreator = (payload: Array<ICoach>): IUpdateCoachesCreator => {
    return {
        type: COACHES_UPDATE_ALL,
        payload
    }
}

export type IAllCoachActions = IUpdateCoachesCreator