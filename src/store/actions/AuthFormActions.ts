import { Dispatch } from "react";
import { setCookie } from "../../cookie/cookie";
import { getAdminStatus, getTokenData } from "../../Rest/fake/fakeRestFunctions";
import { AUTH_SET_ERROR_MESSAGE_FORM, AUTH_SET_LOGIN_MESSAGE, AUTH_SET_PASSWORD_MESSAGE, AUTH_SET_VALID_FORM } from "./ActionTypes";
import { AllAppActions, changeAdminStatus } from "./AppActions";


export const loginUser = (login: string, password: string) => {
    return async (dispatch: Dispatch<IAllAuthFormAtctions | AllAppActions>) => {
        try {
            let isAdmin = false
            const tokenData = await getTokenData(login, password)
            const accessToken = tokenData.access_token
    
            if (accessToken) {
                /// Если пришел токен
                isAdmin = await getAdminStatus(accessToken)  
                
                if (isAdmin) {
                    setCookie('_access_user_token_bf', `${accessToken}`)
                    dispatch(changeAdminStatus(isAdmin))
                } else {
                    dispatch(setErrorMessageForm('Неверный логин / пароль'))
                    dispatch(changeAdminStatus(false))
                }
    
            } else {
                /// Если в ответе нет поля accessToken или оно = ''
                dispatch(setErrorMessageForm('Пользователь не существует'))
            } 

        } catch(e) {
            console.log(e)
        }
        
    }
}


type ISetErrorMessageForm = {
    type: typeof AUTH_SET_ERROR_MESSAGE_FORM,
    payload: string
}
export const setErrorMessageForm = (value: string): ISetErrorMessageForm => {
    return {
        type: AUTH_SET_ERROR_MESSAGE_FORM,
        payload: value
    }
}

type ISetValidForm = {
    type: typeof AUTH_SET_VALID_FORM
}
export const setValidForm = (): ISetValidForm => {
    return {
        type: AUTH_SET_VALID_FORM
    }
}

type ISetLoginMessage = {
    type: typeof AUTH_SET_LOGIN_MESSAGE,
    payload: string
}
export const setLoginMessage = (message: string): ISetLoginMessage => {
    return {
        type: AUTH_SET_LOGIN_MESSAGE,
        payload: message
    }
}

type ISetPasswordMessage = {
    type: typeof AUTH_SET_PASSWORD_MESSAGE,
    payload: string
}
export const setPasswordMessage = (message: string): ISetPasswordMessage => {
    return {
        type: AUTH_SET_PASSWORD_MESSAGE,
        payload: message
    }
}

export type IAllAuthFormAtctions = ISetLoginMessage | ISetValidForm | ISetPasswordMessage | ISetErrorMessageForm
