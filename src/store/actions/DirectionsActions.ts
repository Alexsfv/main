import { Dispatch } from "react"
import { getCookie } from "../../cookie/cookie"
import { getDirections } from "../../Rest/fake/fakeRestFunctions"
import { IDirection, IElementDirectionType } from "../../types/types"
import { DIRECTION_UPDATE_ALL} from "./ActionTypes"
import { IRootState } from "../reducers/rootReducer"
import { sortDirectionsByTitle, sortDirectionsById, getCheckedIds } from "../../Rest/convertFunctions"


export const updateDirections = () => {
    return async (dispatch: Dispatch<IAllDirectionsActions>) => {
        try {
            const access_token = getCookie('_access_user_token_bf')
            const directions = await getDirections(access_token)
            const sortedDirectionsByTitle = sortDirectionsByTitle(directions)
            dispatch(updateDirectionsCreator(sortedDirectionsByTitle)) 
        } catch(e) {
            console.log(e)
        }
    }
}

export const addDirection = (elems: IElementDirectionType) => {
    return async (dispatch: Dispatch<IAllDirectionsActions>, getState: () => IRootState) => {
        try {
            const directions = [...getState().directions.directions]
            const sortedDirectionsById = sortDirectionsById(directions)
    
            const lastId = sortedDirectionsById[sortedDirectionsById.length - 1].id
            const disciplinesInclude = getCheckedIds(elems, 'discipline-')
            const direction = {
                id: lastId + 1,
                title: elems.title.value,
                includes: disciplinesInclude
            }
            directions.push(direction)
    
            const sortedDirections = sortDirectionsByTitle(directions)
            dispatch(updateDirectionsCreator(sortedDirections)) 
        } catch(e) {
            console.log(e)
        }
    }
}

export const editDirection = (elems: IElementDirectionType, directionEdit: IDirection) => {
    return async (dispatch: Dispatch<IAllDirectionsActions>, getState: () => IRootState) => {
        try {
            const directions = [...getState().directions.directions]
            const withoutOldDirection = directions.filter(direction => direction.id !== directionEdit.id)
            const disciplinesInclude = getCheckedIds(elems, 'discipline-')
    
            const direction = {
                id: directionEdit.id,
                title: elems.title.value,
                includes: disciplinesInclude
            }
    
            const newDirections = withoutOldDirection.concat(direction)
            const sortedDirectionsByTitle = sortDirectionsByTitle(newDirections)
            dispatch(updateDirectionsCreator(sortedDirectionsByTitle)) 
        } catch(e) {
            console.log(e)
        }
    }
}

type IUpdateDirectionsCreator = {
    type: typeof DIRECTION_UPDATE_ALL
    payload: Array<IDirection>
}
const updateDirectionsCreator = (payload: Array<IDirection>): IUpdateDirectionsCreator => {
    return {
        type: DIRECTION_UPDATE_ALL,
        payload
    }
}

export type IAllDirectionsActions = IUpdateDirectionsCreator