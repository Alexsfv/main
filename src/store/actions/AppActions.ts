import { Dispatch } from "react"
import { getCookie } from "../../cookie/cookie"
import { getAdminStatus } from "../../Rest/fake/fakeRestFunctions"
import { APP_CHANGE_ADMIN_STATUS } from "./ActionTypes"
import { IAllAuthFormAtctions } from './AuthFormActions'
import { setErrorMessageForm } from './AuthFormActions'

export const checkStatusUser = () => {
    return async (dispatch: Dispatch<AllAppActions | IAllAuthFormAtctions>) => {
        try {
            let isAdmin: boolean = false
            const accessToken = getCookie('_access_user_token_bf')
    
            if (accessToken) {
                /// если есть токен в куках
                isAdmin = await getAdminStatus(accessToken)   
                
                if (isAdmin) {
                    dispatch(changeAdminStatus(isAdmin))
                } else {
                    dispatch(setErrorMessageForm('Неверный логин / пароль'))
                    dispatch(changeAdminStatus(false))
                }
    
            } else {
                /// если токена в куках нет или токен = ''
                dispatch(changeAdminStatus(false))
            }

        } catch(e) {
            console.log(e)
        }
        
    }
}

export type IChangeAdminStatus = {
    type: typeof APP_CHANGE_ADMIN_STATUS,
    payload: boolean
}
export const changeAdminStatus = (status: boolean): IChangeAdminStatus => {
    return {
        type: APP_CHANGE_ADMIN_STATUS,
        payload: status
    }
}

export type AllAppActions = IChangeAdminStatus