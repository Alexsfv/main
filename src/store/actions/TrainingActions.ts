import { Dispatch } from "react"
import { getCookie } from "../../cookie/cookie"
import { trainingsWithDate, ITrainingsWithDate, sortTrainingsById } from "../../Rest/convertFunctions"
import { getTrainings } from "../../Rest/fake/fakeRestFunctions"
import { IElementTrainingType, ITimeTraining, ITraining } from "../../types/types"
import { IRootState } from "../reducers/rootReducer"
import { TRAINING_UPDATE_TRAININGS } from "./ActionTypes"
import { trainingTemplate } from '../../Rest/fake/fakeRestFunctions'

export function getTrainingsArray() {
    return async (dispatch: Dispatch<AllTrainigActions>) => {
        try {
            const access_token = getCookie('_access_user_token_bf')
            const trainings = await getTrainings(access_token)
            const trainingsDateObj = trainingsWithDate(trainings)
            const trainingsDateKeys = Object.keys(trainingsDateObj)

            dispatch(updateTrainings(trainings, trainingsDateKeys, trainingsDateObj))
        } catch(e) {
            console.log(e)
        }
    }
} 

export function addTraining(elems: IElementTrainingType, time: ITimeTraining) {
    return async (dispatch: Dispatch<AllTrainigActions>, getState: () => IRootState) => {
        try {
            const coachId = +elems.coach.value
            const deleted = elems.deleted.value === 'true'
            const completed = elems.completed.value === 'true'
            const free = elems.free.value === 'true'            
    
            const trainings = [...getState().trainings.trainings]
            const coaches = [...getState().coaches.coaches]
            const chosenCoach = coaches.filter(coach => coach.id === coachId)[0]
    
            const sortedTrainingsById = sortTrainingsById(trainings)
            const lastId = sortedTrainingsById[sortedTrainingsById.length - 1].id
    
            const disciplineId = +elems.discipline.value
    
            const newTraining = {
                ...trainingTemplate,
                id: lastId + 1,
                start: time.startTime,
                finish: time.finishTime,
                title: elems.title.value,
                description: elems.description.value,
                "store": {
                    "id": 1
                },
                discipline: {
                    id: disciplineId
                },
                coach: chosenCoach,
                completed: completed,
                deleted: deleted,
                duration: +elems.duration.value,
                free: free,
                placesLimit: +elems.placesLimit.value
            }
            trainings.push(newTraining)
    
            const trainingsDateObj = trainingsWithDate(trainings)
            const trainingsDateKeys = Object.keys(trainingsDateObj)

            dispatch(updateTrainings(trainings, trainingsDateKeys, trainingsDateObj))
        } catch(e) {
            console.log(e)
        }
    }
}


export function editTraining(elems: IElementTrainingType, trainigEdit: ITraining, time: ITimeTraining) {
    return async (dispatch: Dispatch<AllTrainigActions>, getState: () => IRootState) => {
        try {
            const coachId = +elems.coach.value
            const deleted = elems.deleted.value === 'true'
            const completed = elems.completed.value === 'true'
            const free = elems.free.value === 'true'
    
            const trainings = [...getState().trainings.trainings]
            const coaches = [...getState().coaches.coaches]
            const chosenCoach = coaches.filter(coach => coach.id === coachId)[0]
    
            const withoutOldTraining = trainings.filter(training => training.id !== trainigEdit.id)
    
            const disciplineId = +elems.discipline.value
    
            const newTraining = {
                ...trainingTemplate,
                id: trainigEdit.id,
                start: time.startTime,
                finish: time.finishTime,
                title: elems.title.value,
                description: elems.description.value,
                "store": {
                    "id": 1
                },
                discipline: {
                    id: disciplineId
                },
                coach: chosenCoach,
                completed: completed,
                deleted: deleted,
                duration: +elems.duration.value,
                free: free,
                placesLimit: +elems.placesLimit.value
            }
    
            const newTrainings = withoutOldTraining.concat(newTraining)
    
            const trainingsDateObj = trainingsWithDate(newTrainings)
            const trainingsDateKeys = Object.keys(trainingsDateObj)

            dispatch(updateTrainings(newTrainings, trainingsDateKeys, trainingsDateObj))
        } catch(e) {
            console.log(e)
        }
    }
}

type IUpdateTrainingsPayload = {
    newTrainings: Array<ITraining>,
    trainingsDateKeys: Array<string>,
    trainingsDateObj: ITrainingsWithDate
}
type IUpdateTrainings = {
    type: typeof TRAINING_UPDATE_TRAININGS
    payload: IUpdateTrainingsPayload
}
const updateTrainings = (newTrainings: Array<ITraining>, trainingsDateKeys: Array<string>, trainingsDateObj: ITrainingsWithDate): IUpdateTrainings => {
    return {
        type: TRAINING_UPDATE_TRAININGS,
        payload: {
            newTrainings,
            trainingsDateKeys,
            trainingsDateObj
        }
    }
}

export type AllTrainigActions = IUpdateTrainings