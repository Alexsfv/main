import { Dispatch } from "react"
import { getCookie } from "../../cookie/cookie"
import { getDisciplines } from "../../Rest/fake/fakeRestFunctions"
import { IDiscipline, IElementDisciplineType } from "../../types/types"
import { DISCIPLINES_UPDATE_ALL } from "./ActionTypes"
import { IRootState } from "../reducers/rootReducer"
import { sortDisciplinesByTitle, sortDisciplinesById, getCheckedIds } from "../../Rest/convertFunctions"


export const updateDisciplines = () => {
    return async (dispatch: Dispatch<IAllDisciplineshActions>) => {
        try {
            const access_token = getCookie('_access_user_token_bf')
            const disciplines = await getDisciplines(access_token)
            const sortedDisciplinesByTitle = sortDisciplinesByTitle(disciplines)
            dispatch(updateDisciplinesCreator(sortedDisciplinesByTitle))   
        } catch(e) {
            console.log(e)
        }
    }
}

export const addDiscipline = (elems: IElementDisciplineType) => {
    return async (dispatch: Dispatch<IAllDisciplineshActions>, getState: () => IRootState) => {
        try {
            const disciplines = [...getState().disciplines.disciplines]
            const sortedDisciplinesById = sortDisciplinesById(disciplines)
    
            const lastId = sortedDisciplinesById[sortedDisciplinesById.length - 1].id
            const coachesInclude = getCheckedIds(elems, 'coach-')
            const discipline = {
                id: lastId + 1,
                title: elems.title.value,
                includes: coachesInclude
            }
            disciplines.push(discipline)
    
            const sortedDisciplines = sortDisciplinesByTitle(disciplines)
            dispatch(updateDisciplinesCreator(sortedDisciplines)) 
        } catch(e) {
            console.log(e)
        }
    }
}

export const editDiscipline = (elems: IElementDisciplineType, disciplineEdit: IDiscipline) => {
    return async (dispatch: Dispatch<IAllDisciplineshActions>, getState: () => IRootState) => {
        try {
            const disciplines = [...getState().disciplines.disciplines]

            const withoutOldDiscipline = disciplines.filter(discipline => discipline.id !== disciplineEdit.id)
            const coachesInclude = getCheckedIds(elems, 'coach-')
            const discipline = {
                id: disciplineEdit.id,
                title: elems.title.value,
                includes: coachesInclude
            }
    
            const newDisciplines = withoutOldDiscipline.concat(discipline)
            const sortedDisciplinesByTitle = sortDisciplinesByTitle(newDisciplines)
            dispatch(updateDisciplinesCreator(sortedDisciplinesByTitle)) 
        } catch(e) {
            console.log(e)
        }
    }
}

type IUpdateDisciplinesCreator = {
    type: typeof DISCIPLINES_UPDATE_ALL
    payload: Array<IDiscipline>
}
const updateDisciplinesCreator = (payload: Array<IDiscipline>): IUpdateDisciplinesCreator => {
    return {
        type: DISCIPLINES_UPDATE_ALL,
        payload
    }
}

export type IAllDisciplineshActions = IUpdateDisciplinesCreator