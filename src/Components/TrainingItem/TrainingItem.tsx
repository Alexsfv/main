import React from 'react'
import { ITraining } from '../../types/types'
import './TrainingItem.scss'

type ITrainingItemProps = {
    training: ITraining
    startMinutes: string
    startHours: string
    finishMinutes: string
    finishHours: string
    isClosing: boolean
    clickEditHandler: () => void
}

const TrainingItem: React.FC<ITrainingItemProps> = ({training, startMinutes, startHours, finishMinutes, finishHours, isClosing, clickEditHandler}) => {

    const statusClasses = ['item-training__status']
    isClosing ? statusClasses.push('error') : statusClasses.push('success')

    return (
        <div className="training__item item-training">
            <div className="item-training__short-info">
                <p className="item-training__time">
                    <span>
                        {startHours}:{startMinutes}
                    </span>
                    <span>
                        {finishHours}:{finishMinutes}
                    </span>
                </p>
                <p className="item-training__load success">
                    {training.clients.length} / {training.placesLimit}
                </p>
            </div>

            <div className="item-training__main-info">
                <p className="item-training__title">
                    {training.title}
                </p>
                <p className="item-training__fullName">
                    {training.coach.fullName}
                </p>
                <p className={statusClasses.join(' ')}>
                    {
                        isClosing ? 'Завершено' : `До завершения: ${training.numberMinutesBeforeClosing} минут`
                    }
                </p>
            </div>

            <button className="item-training__edit" onClick={clickEditHandler}>
                <i className="fa fa-wrench" aria-hidden="true"></i>
            </button>
            
        </div>
    )
}

export default TrainingItem