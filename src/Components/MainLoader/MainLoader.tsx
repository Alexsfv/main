import React from 'react'
import Loader from '../Loader/Loader'
import './MainLoader.scss'

const MainLoader: React.FC<{}> = () => {

    return (
        <div className="lds-fullscreen">
            <Loader />
        </div>
    )
}

export default MainLoader