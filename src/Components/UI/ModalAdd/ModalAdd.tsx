import React from 'react'
import './ModalAdd.scss'

type IModalProps = {
    children: React.ReactNode
    show: boolean
    title: string
    onClose: () => void
}
const ModalAdd: React.FC<IModalProps> = ({ children, title, show, onClose }) => {

    return (
        <>
            {
                show
                    ?   <div className="add">
                            <div className="add__body">
                
                                <i className="fa fa-times add__close" aria-hidden="true" onClick={onClose}></i>
                                <p className="add__title">{title}</p>
                
                                {children}
                                
                            </div>
                        </div>
                    :   null
            }
        </>
    )
}

export default ModalAdd