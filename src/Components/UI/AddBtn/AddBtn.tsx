import React from 'react'
import './AddBtn.scss'

type IAddBtnProps = {
    title: string
    onClick: () => void
}

const AddBtn: React.FC<IAddBtnProps> = ({title, onClick}) => {

    return (
        <button className="add-btn" onClick={onClick}>
            {title}
            <i className="fa fa-plus-circle" aria-hidden="true"></i>
        </button>
    )
}

export default AddBtn