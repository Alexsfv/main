import React, { Dispatch } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import './AuthForm.scss'
import { IRootState } from '../../store/reducers/rootReducer'
import { IAllAuthFormAtctions, setLoginMessage, setPasswordMessage, setValidForm } from '../../store/actions/AuthFormActions'


type IOwnProps = {
    loginUser: (login: string, password: string) => void
    submitText: string
    title: string
}

interface IFormElements extends HTMLCollection  {
    login: HTMLInputElement
    password: HTMLInputElement
}

const AuthForm: React.FC<IOwnProps> = (props) => {

    const { isValidForm,
            loginErrorMessage, 
            passwordErrorMessage,
            formErrorMessage
    } = useSelector((state: IRootState) => state.authForm)

    const dispatch = useDispatch<Dispatch<IAllAuthFormAtctions>>()

    const isValidateForm = (login: string, password: string): boolean => {
        let isValid = true
        if (login.length === 0) {
            dispatch(setLoginMessage('Введите логин'))
            isValid = false
        }
        if (password.length === 0) {
            dispatch(setPasswordMessage('Введите пароль'))
            isValid = false
        }
        return isValid
    }

    const submitHandler = (e: React.SyntheticEvent): void => {
        e.preventDefault()
        const form = e.target as HTMLFormElement

        const elements: IFormElements = form.elements as any
        const login = elements.login.value
        const password = elements.password.value

        if (!isValidateForm(login, password)) {
            return undefined
        }

        props.loginUser(login, password)
    }

    const changeInputHandler = (): void => {
        if (!isValidForm) {
            dispatch(setValidForm())
        }
    }

    const loginClasses = ['form__input-message']
    if (loginErrorMessage.length > 0) {
        loginClasses.push('active')
    }

    const passwordClasses = ['form__input-message']
    if (passwordErrorMessage.length > 0) {
        passwordClasses.push('active')
    }


    return (
        <div className="form">
            <p className="form__title">{props.title}</p>
            <form className="form__body" onSubmit={submitHandler}>

                <div className="form__input-field">
                    <div className="form__input-with-icon">
                        <input 
                            type="text" 
                            className="form__input" 
                            name="login" 
                            placeholder="Логин"
                            onChange={changeInputHandler}
                        />
                        <i className="fa fa-user form__input-icon" aria-hidden="true"></i>
                    </div>
                    <span className={loginClasses.join(' ')}>{loginErrorMessage}</span>
                </div>

                <div className="form__input-field">
                    <div className="form__input-with-icon">
                        <input 
                            type="password" 
                            className="form__input" 
                            name="password" 
                            placeholder="Пароль"
                            onChange={changeInputHandler}
                        />
                        <i className="fa fa-lock form__input-icon" aria-hidden="true"></i>
                    </div>
                    <span className={passwordClasses.join(' ')}>{passwordErrorMessage}</span>
                </div>

                <p className="form__error-message">{formErrorMessage}</p>

                <input type="submit" value={props.submitText} className="form__submit" disabled={!isValidForm}/>
            </form>
        </div>
    )
}

export default AuthForm