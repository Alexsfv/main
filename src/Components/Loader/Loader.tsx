import React from 'react'
import './Loader.scss'

const Loader: React.FC<{}> = () => {

    return (
            <div className="lds-grid">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
    )
}

export default Loader