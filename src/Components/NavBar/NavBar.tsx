import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { deleteCookie } from '../../cookie/cookie'
import { changeAdminStatus } from '../../store/actions/AppActions'
import './NavBar.scss'

const NavBar: React.FC<{}> = () => {

    const [isOpen, setOpen] = useState(false)
    const dispatch = useDispatch()

    const openBurgerMenu = () => {
        setOpen(true)
    }

    const closeBurgerMenu = (e: React.MouseEvent) => {
        e.stopPropagation()
        setOpen(false)
    }

    const logoutHandler = () => {
        deleteCookie('_access_user_token_bf')
        dispatch(changeAdminStatus(false))
    }

    const navClasses = ['nav']

    if (isOpen) {
        navClasses.push('open-burger')
    }

    

    return (
        <div className={navClasses.join(' ')}>
            <div className="nav__bg" onClick={closeBurgerMenu}>
                <div className="nav__fullscreen-bg"></div>
                <div className="nav__body" onClick={e => e.stopPropagation()}>

                    <i className="fa fa-times nav__close-btn" aria-hidden="true" onClick={closeBurgerMenu}></i>

                    <ul className="nav__main-links-container">
                        <li onClick={closeBurgerMenu}>
                            <NavLink to="/admin" className="nav__main-link" activeClassName="active" exact>Главная</NavLink>
                        </li>

                        <li onClick={closeBurgerMenu}>
                            <NavLink to="/admin/training" className="nav__main-link" activeClassName="active">Тренировки</NavLink>
                        </li>

                        <li onClick={closeBurgerMenu}>
                            <NavLink to="/admin/coaches" className="nav__main-link" activeClassName="active">Тренеры</NavLink>
                        </li>

                        <li onClick={closeBurgerMenu}>
                            <NavLink to="/admin/disciplines" className="nav__main-link" activeClassName="active">Программы</NavLink>
                        </li>

                        <li onClick={closeBurgerMenu}>
                            <NavLink to="/admin/directions" className="nav__main-link" activeClassName="active">Направления</NavLink>
                        </li>

                        <li onClick={logoutHandler}>
                            <button className="nav__main-link">Выйти</button>
                        </li>
                    </ul>
                    

                </div>
            </div>
            <i className="fa fa-bars nav__open-btn" aria-hidden="true" onClick={openBurgerMenu}></i>
        </div>
    )
}

export default NavBar