import React from 'react'
import { useSelector } from 'react-redux'
import { ITrainingsWithDate, parseDate } from '../../Rest/convertFunctions'
import { IRootState } from '../../store/reducers/rootReducer'
import TrainingItem from '../TrainingItem/TrainingItem'
import { ITraining } from '../../types/types'

type ITrainingBodyProps = {
    clickEditHandler: (training: ITraining) => void
}

const TrainingBody: React.FC<ITrainingBodyProps> = ({clickEditHandler}) => {

    const trainingsWithDate = useSelector<IRootState>(state => state.trainings.trainingsWithDate) as ITrainingsWithDate
    const dateKeys = useSelector<IRootState>(state => state.trainings.dateKeys) as Array<string>
    
    return (
        <>
            {
                dateKeys.map(dateKey => {
                    let [hours, day, month, year] = dateKey.split('-')

                    return (
                        <div className="training__body" key={dateKey}>
                            <p className="training__body-time">
                                {`${hours}:00 ${day}-${month}-${year}`}
                            </p>
    
                            {
                                trainingsWithDate[dateKey].map(training => {
                                    
                                    const [startMinutes, startHours] = parseDate(training.start)
                                    const [finishMinutes, finishHours] = parseDate(training.finish)

                                    const isClosing = training.numberMinutesBeforeClosing > 0 ? false : true
                                    
                                    return <TrainingItem 
                                        training={training}
                                        startMinutes={startMinutes}
                                        startHours={startHours}
                                        finishMinutes={finishMinutes}
                                        finishHours={finishHours}
                                        isClosing={isClosing}
                                        clickEditHandler={() => clickEditHandler(training)}
                                        key={training.id}
                                />
                                })
                            }
    
                        </div>
                    )

                  })
            }
        </>
    )
}

export default TrainingBody
