export function getCookie(name: string): '' | null {
    let result: string | null = null
    const cookie = document.cookie
    const cookieProp = cookie.split('; ')
    cookieProp.forEach(cookiePair => {
        const [key, value] = cookiePair.split('=')
        if (key === name) {
            result = value
        }
    })
    return result
}

export function setCookie(name: string, value: string, maxAge: number = 86398): void {
    document.cookie = `${name}=${value}; max-age=${maxAge}`    
}

export function deleteCookie(name: string): void {
    document.cookie = `${name}=; path=/; expires=-1; max-age=-1`
}