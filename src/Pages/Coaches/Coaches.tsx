import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AddBtn from '../../Components/UI/AddBtn/AddBtn'
import ModalAdd from '../../Components/UI/ModalAdd/ModalAdd'
import { lockBodyElement, unlockBodyElement } from '../../Rest/convertFunctions'
import { addCoach, editCoach, updateCoaches } from '../../store/actions/CoachesActions'
import { IRootState } from '../../store/reducers/rootReducer'
import { ICoach, IElementCoachType } from '../../types/types'
import './Coaches.scss'

const Coaches: React.FC<{}> = () => {

    const [isCreating, setCreating] = useState<boolean>(false)
    const [isEdit, setIsEdit] = useState<boolean>(false)
    const [coachEdit, setCoachEdit] = useState<ICoach>({} as ICoach)

    const coaches = useSelector<IRootState>(state => state.coaches.coaches) as Array<ICoach>
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(updateCoaches())
    },[]) // eslint-disable-line react-hooks/exhaustive-deps

    const openAdd = () => {
        setCreating(true)
        lockBodyElement()
    }

    const closeModals = () => {
        setCreating(false)
        setIsEdit(false)
        unlockBodyElement()
    }

    const addCoachSubmitHandler = (e: React.FormEvent) => {
        e.preventDefault()
        const form = e.target as HTMLFormElement
        const elems: IElementCoachType = form.elements as any
        
        dispatch(addCoach(elems))
    
        form.reset()
        closeModals()
    }

    const openEditCoach = (coach: ICoach) => {
        setCoachEdit(coach)
        setIsEdit(true)
        lockBodyElement()
    }

    const editFormHandler = (e: React.FormEvent) => {
        e.preventDefault()
        const form = e.target as HTMLFormElement
        const elems: IElementCoachType = form.elements as any
        
        dispatch(editCoach(elems, coachEdit))
    
        form.reset()
        closeModals()
        setCoachEdit({} as ICoach)
    }

    return (
        <>
            <section className="coaches">
                <div className="container">
                    <AddBtn title="Добавить тренера" onClick={openAdd}/>
                    <p className="coaches__title">Список тренеров</p>

                    <div className="row w-100 justify-content-center mx-auto">

                        {
                            coaches.map(coach => (
                                <div className="card mx-4 my-4 " style={{width: '18rem'}} key={coach.id}>
                                    <img className="card-img-top" src="https://via.placeholder.com/150" alt="Avatar" />
                                    <div className="card-body d-flex flex-column">
                                        <h5 className="card-title">{coach.fullName}</h5>
                                        <p className="card-text">
                                            <span className="d-block my-1">Email: {coach.email}</span>
                                            <span className="d-block my-1">Тел.: {coach.phone}</span>
                                            <span className="d-block my-1">{coach.description}</span>
                                        </p>
                                        <button className="btn btn-primary mt-auto" onClick={() => openEditCoach(coach)}>Редактировать</button>
                                    </div>
                                </div>
                            ))
                        }

                    </div>
                </div>


                <ModalAdd
                    show={isCreating}
                    title="Добавление тренера"
                    onClose={closeModals}
                >
                    <form className="add__form" onSubmit={addCoachSubmitHandler}>
                        <div className="add__form-field">
                            <label htmlFor="add-firstName">Имя тренера</label>
                            <input type="text" placeholder="Введите имя" name="firstName" id="add-firstName" required/>
                        </div>

                        <div className="add__form-field">
                            <label htmlFor="add-secondName">Фамилия тренера</label>
                            <input type="text" placeholder="Введите фамилию" name="secondName" id="add-secondName" required/>
                        </div>

                        <div className="add__form-field">
                            <label htmlFor="add-phone">Номер телефона тренера</label>
                            <input type="tel" placeholder="Введите телефон" name="phone" id="add-phone"/>
                        </div>

                        <div className="add__form-field">
                            <label htmlFor="add-email">Электронная почта тренера</label>
                            <input type="email" placeholder="Введите eMail" name="email" id="add-email"/>
                        </div>

                        <div className="add__form-field">
                            <label htmlFor="">Выберите пол</label>
                            <div className="training__form-radio">
                                <input type="radio" name="sex" value="man" required={true}/> Муж.
                                <input type="radio" name="sex" value="woman" defaultChecked={true}/> Жен.
                            </div>
                        </div>

                        <div className="add__form-field">
                            <label htmlFor="add-description">Дополнительная информация о тренере</label>
                            <input type="text" placeholder="Введите описание" name="description" id="add-description"/>
                        </div>

                        <input type="submit" value="Добавить тренера" className="add__submit-btn"/>
                    </form>
                </ModalAdd>


                <ModalAdd
                    show={isEdit}
                    title="Редактировать информацию"
                    onClose={closeModals}
                >
                    <form className="add__form" onSubmit={editFormHandler}>
                        <div className="add__form-field">
                            <label htmlFor="add-firstName">Имя тренера</label>
                            <input type="text" placeholder="Введите имя" name="firstName" id="add-firstName" required defaultValue={coachEdit.firstName}/>
                        </div>

                        <div className="add__form-field">
                            <label htmlFor="add-secondName">Фамилия тренера</label>
                            <input type="text" placeholder="Введите фамилию" name="secondName" id="add-secondName" required defaultValue={coachEdit.surname}/>
                        </div>

                        <div className="add__form-field">
                            <label htmlFor="add-phone">Номер телефона тренера</label>
                            <input type="tel" placeholder="Введите телефон" name="phone" id="add-phone" defaultValue={coachEdit.phone || ''}/>
                        </div>

                        <div className="add__form-field">
                            <label htmlFor="add-email">Электронная почта тренера</label>
                            <input type="email" placeholder="Введите eMail" name="email" id="add-email"  defaultValue={coachEdit.email || ''}/>
                        </div>

                        <div className="add__form-field">
                            <label htmlFor="">Выберите пол</label>
                            <div className="training__form-radio">
                                <input type="radio" name="sex" value="man" required={true} defaultChecked={coachEdit.sex === 'man'}/> Муж.
                                <input type="radio" name="sex" value="woman" defaultChecked={coachEdit.sex === 'woman'}/> Жен.
                            </div>
                        </div>

                        <div className="add__form-field">
                            <label htmlFor="add-description">Дополнительная информация о тренере</label>
                            <input type="text" placeholder="Введите описание" name="description" id="add-description" defaultValue={coachEdit.description || ''}/>
                        </div>

                        <input type="submit" value="Изменить информацию" className="add__submit-btn"/>
                    </form>
                </ModalAdd>
            </section>
        </>
    )
}

export default Coaches