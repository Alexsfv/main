import React from 'react'
import { useDispatch } from 'react-redux'
import AuthForm from '../../Components/AuthForm/AuthForm'
import { loginUser } from '../../store/actions/AuthFormActions'
import './Auth.scss'

const Login: React.FC<{}> = () => {

    const dispatch = useDispatch()

    const authSubmitHandler = (login: string, password: string): void => {
        dispatch(loginUser(login, password))       
    }

    return (
        <>
            <section className="auth">
                <div className="container auth-container">

                    <AuthForm 
                        title="Авторизация" 
                        submitText="вход"
                        loginUser={authSubmitHandler}
                    />

                </div>
            </section>
        
        </>
    )
}

export default Login