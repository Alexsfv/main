import React from 'react'
import './MainAdmin.scss'

const MainAdmin:React.FC<{}> = () => {

    return (
        <section className="main-admin">
            <div className="container">
                <h2 className="main-admin__title">Добро пожаловать!</h2>
                <p className="main-admin__description">Выберите одну из категорий для редактирования</p>
            </div>
        </section>
    )
}

export default MainAdmin