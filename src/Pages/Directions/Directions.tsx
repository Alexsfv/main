import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AddBtn from '../../Components/UI/AddBtn/AddBtn'
import ModalAdd from '../../Components/UI/ModalAdd/ModalAdd'
import { lockBodyElement, unlockBodyElement } from '../../Rest/convertFunctions'
import { addDirection, editDirection, updateDirections } from '../../store/actions/DirectionsActions'
import { IRootState } from '../../store/reducers/rootReducer'
import { IDirection, IDiscipline, IElementDirectionType } from '../../types/types'
import './Directions.scss'

const Directions: React.FC<{}> = () => {

    const [isCreating, setCreating] = useState<boolean>(false)
    const [isEdit, setIsEdit] = useState<boolean>(false)
    const [directionEdit, setDirectionEdit] = useState<IDirection>({} as IDirection)

    const directions = useSelector<IRootState>(state => state.directions.directions) as Array<IDirection>
    const disciplines = useSelector<IRootState>(state => state.disciplines.disciplines) as Array<IDiscipline>
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(updateDirections())
    },[]) // eslint-disable-line react-hooks/exhaustive-deps

    const openAdd = () => {
        setCreating(true)
        lockBodyElement()
    }

    const closeModals = () => {
        setCreating(false)
        setIsEdit(false)
        unlockBodyElement()
    }

    const addDirectionSubmitHandler = (e: React.FormEvent) => {
        e.preventDefault()
        const form = e.target as HTMLFormElement
        const elems: IElementDirectionType = form.elements as any
        
        dispatch(addDirection(elems))
    
        form.reset()
        closeModals()
    }

    const openEditDirection = (direction: IDirection) => {
        setDirectionEdit(direction)
        setIsEdit(true)
        lockBodyElement()
    }

    const editFormHandler = (e: React.FormEvent) => {
        e.preventDefault()
        const form = e.target as HTMLFormElement
        const elems: IElementDirectionType = form.elements as any
        
        dispatch(editDirection(elems, directionEdit))
    
        form.reset()
        closeModals()
        setDirectionEdit({} as IDirection)
    }

    const getDisciplineTitle = (disciplineId: number): string | null => {
        const activeDiscipline = disciplines.filter(discipline => discipline.id === disciplineId)
        const disciplineName = activeDiscipline.length > 0 ? activeDiscipline[0].title : null 
        return disciplineName
    }

    return (
        <>
            <section className="directions">
                <div className="container">
                    <AddBtn title="Добавить направление" onClick={openAdd}/>
                    <p className="directions__title">Список направлений</p>

                    <div className="row w-100 justify-content-center mx-auto">

                        {
                            directions.map(direction => (
                                <div className="card mx-4 my-4 " style={{width: '18rem'}} key={direction.id}>
                                    <div className="card-body d-flex flex-column">
                                        <h4 className="card-title text-center font-weight-bold">{direction.title}</h4>
                                        <p className="font-weight-bold text-center">Список программ</p>
                                        <p className="card-text">
                                            {
                                                direction.includes
                                                    ?   direction.includes.map(disciplineId => (
                                                                <span className="my-1 d-block" key={disciplineId}>
                                                                    {getDisciplineTitle(disciplineId)}
                                                                </span>
                                                            )
                                                        )
                                                    :   null 
                                            }
                                        </p>
                                        <button className="btn btn-primary mt-auto" onClick={() => openEditDirection(direction)}>Редактировать</button>
                                    </div>
                                </div>
                            ))
                        }

                    </div>
                </div>


                <ModalAdd
                    show={isCreating}
                    title="Создание направления"
                    onClose={closeModals}
                >
                    <form className="add__form" onSubmit={addDirectionSubmitHandler}>
                        <div className="add__form-field">
                            <label htmlFor="add-title">Название направления</label>
                            <input type="text" placeholder="Введите название" name="title" id="add-title" required/>
                        </div>

                        <p className="add__form-text">Выберите программы, которые входят в направление</p>

                        <div className="add__checkbox-field">
                            {
                                disciplines.map(discipline => (
                                        <div className="add__checkbox-item" key={discipline.id}>
                                            <input type="checkbox" name={`${discipline.title}`} id={`discipline-${discipline.id}`}/>
                                            <label htmlFor={`discipline-${discipline.id}`}>{discipline.title}</label>
                                        </div>
                                    )
                                )
                            }                                        
                        </div>

                        <input type="submit" value="Добавить направление" className="add__submit-btn"/>
                    </form>
                </ModalAdd>

                    
                <ModalAdd
                    show={isEdit}
                    title="Редактирование направления"
                    onClose={closeModals}
                >
                    <form className="add__form" onSubmit={editFormHandler}>
                        <div className="add__form-field">
                            <label htmlFor="add-title">Название направления</label>
                            <input type="text" placeholder="Введите название" name="title" id="add-title" required defaultValue={directionEdit.title}/>
                        </div>

                        <p className="add__form-text">Выберите программы, которые входят в направление</p>

                        <div className="add__checkbox-field">
                            {
                                disciplines.map(discipline => {
                                    const isChecked = directionEdit.includes ? directionEdit.includes.includes(discipline.id) : false
                                    return (
                                        <div className="add__checkbox-item" key={discipline.id}>
                                            <input type="checkbox" name={`${discipline.title}`} id={`discipline-${discipline.id}`} defaultChecked={isChecked}/>
                                            <label htmlFor={`discipline-${discipline.id}`}>{discipline.title}</label>
                                        </div>
                                    )
                                }
                                )
                            }                                        
                        </div>

                        <input type="submit" value="Изменить направление" className="add__submit-btn"/>
                    </form>
                </ModalAdd>  
            </section>
        </>
    )
}

export default Directions