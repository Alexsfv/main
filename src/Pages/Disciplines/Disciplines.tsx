import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AddBtn from '../../Components/UI/AddBtn/AddBtn'
import ModalAdd from '../../Components/UI/ModalAdd/ModalAdd'
import { lockBodyElement, unlockBodyElement } from '../../Rest/convertFunctions'
import { addDiscipline, editDiscipline, updateDisciplines } from '../../store/actions/DisciplinesActions'
import { IRootState } from '../../store/reducers/rootReducer'
import { ICoach, IDiscipline } from '../../types/types'
import { IElementDisciplineType } from '../../types/types'
import './Disciplines.scss'

const Disciplines: React.FC<{}> = () => {

    const [isCreating, setCreating] = useState<boolean>(false)
    const [isEdit, setIsEdit] = useState<boolean>(false)
    const [disciplineEdit, setDisciplineEdit] = useState<IDiscipline>({} as IDiscipline)

    const disciplines = useSelector<IRootState>(state => state.disciplines.disciplines) as Array<IDiscipline>
    const coaches = useSelector<IRootState>(state => state.coaches.coaches) as Array<ICoach>
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(updateDisciplines())
    },[]) // eslint-disable-line react-hooks/exhaustive-deps

    const openAdd = () => {
        setCreating(true)
        lockBodyElement()
    }

    const closeModals = () => {
        setCreating(false)
        setIsEdit(false)
        unlockBodyElement()
    }

    const addDisciplineSubmitHandler = (e: React.FormEvent) => {
        e.preventDefault()
        const form = e.target as HTMLFormElement
        const elems: IElementDisciplineType = form.elements as any
        
        dispatch(addDiscipline(elems))
    
        form.reset()
        closeModals()
    }

    const openEditDiscipline = (discipline: IDiscipline) => {
        setDisciplineEdit(discipline)
        setIsEdit(true)
        lockBodyElement()
    }

    const editFormHandler = (e: React.FormEvent) => {
        e.preventDefault()
        const form = e.target as HTMLFormElement
        const elems: IElementDisciplineType = form.elements as any
        
        dispatch(editDiscipline(elems, disciplineEdit))
    
        form.reset()
        closeModals()
        setDisciplineEdit({} as IDiscipline)
    }

    const getCoachFullname = (coachId: number): string | null => {
        const activeCoach = coaches.filter(coach => coach.id === coachId)
        const coachFullName = activeCoach.length > 0 ? activeCoach[0].fullName : null 
        return coachFullName
    }

    return (
        <>
            <section className="disciplines">
                <div className="container">
                    <AddBtn title="Добавить программу" onClick={openAdd}/>
                    <p className="disciplines__title">Список программ</p>

                    <div className="row w-100 justify-content-center mx-auto">

                        {
                            disciplines.map(discipline => (
                                <div className="card mx-4 my-4 " style={{width: '18rem'}} key={discipline.id}>
                                    <div className="card-body d-flex flex-column">
                                        <h4 className="card-title text-center font-weight-bold">{discipline.title}</h4>
                                        <p className="font-weight-bold text-center">Список тренеров</p>
                                        <p className="card-text">
                                            {
                                                discipline.includes
                                                    ?   discipline.includes.map(coachId => (
                                                                <span className="my-1 d-block" key={coachId}>
                                                                    {getCoachFullname(coachId)}
                                                                </span>
                                                            )
                                                        )
                                                    :   null 
                                            }
                                        </p>
                                        <button className="btn btn-primary mt-auto" onClick={() => openEditDiscipline(discipline)}>Редактировать</button>
                                    </div>
                                </div>
                            ))
                        }

                    </div>
                </div>

                
                    
                <ModalAdd
                    show={isCreating}
                    title="Создание программу"
                    onClose={closeModals}
                >
                    <form className="add__form" onSubmit={addDisciplineSubmitHandler}>
                        <div className="add__form-field">
                            <label htmlFor="add-title">Название программы</label>
                            <input type="text" placeholder="Введите название" name="title" id="add-title" required/>
                        </div>

                        <p className="add__form-text">Выберите тренеров, преподающих данную программу</p>
                        <div className="add__checkbox-field">
                            {
                                coaches.map(coach => (
                                        <div className="add__checkbox-item" key={coach.id}>
                                            <input type="checkbox" name={`${coach.fullName}`} id={`coach-${coach.id}`}/>
                                            <label htmlFor={`coach-${coach.id}`}>{coach.fullName}</label>
                                        </div>
                                    )
                                )
                            }                                        
                        </div>

                        <input type="submit" value="Добавить программу" className="add__submit-btn"/>
                    </form>
                </ModalAdd>
                
                    
                <ModalAdd
                    show={isEdit}
                    title="Редактирование программы"
                    onClose={closeModals}
                >
                    <form className="add__form" onSubmit={editFormHandler}>
                            <div className="add__form-field">
                                <label htmlFor="add-title">Название программы</label>
                                <input type="text" placeholder="Введите название" name="title" id="add-title" required defaultValue={disciplineEdit.title}/>
                            </div>

                            <p className="add__form-text">Выберите тренеров, преподающих данную программу</p>
                            <div className="add__checkbox-field">
                                {
                                    coaches.map(coach => {
                                        const isChecked = disciplineEdit.includes ? disciplineEdit.includes.includes(coach.id) : false
                                        return (
                                            <div className="add__checkbox-item" key={coach.id}>
                                                <input type="checkbox" name={`${coach.fullName}`} id={`coach-${coach.id}`} defaultChecked={isChecked}/>
                                                <label htmlFor={`coach-${coach.id}`}>{coach.fullName}</label>
                                            </div>
                                        )
                                    }
                                    )
                                }                                        
                            </div>
                            <input type="submit" value="Изменить программу" className="add__submit-btn"/>
                        </form>
                </ModalAdd>  
            </section>
        </>
    )
}

export default Disciplines