import React, { useEffect, useState } from 'react'
import './Training.scss'
import '../../Components/DatePicker/datepicker.scss'
import Loader from '../../Components/Loader/Loader'
import { useDispatch, useSelector } from 'react-redux'
import { IRootState } from '../../store/reducers/rootReducer'
import { getTrainingsArray, addTraining, editTraining } from '../../store/actions/TrainingActions'
import TrainingBody from '../../Components/TrainingBody/TrainingBody'
import AddBtn from '../../Components/UI/AddBtn/AddBtn'
import { ITraining, IElementTrainingType, IDiscipline, ICoach } from '../../types/types'
import DatePicker from 'react-datepicker'
import ru from 'date-fns/locale/ru';
import { lockBodyElement, unlockBodyElement } from '../../Rest/convertFunctions'
import ModalAdd from '../../Components/UI/ModalAdd/ModalAdd'


const Training: React.FC<{}> = () => {

    const [startDate, setStartDate] = useState(new Date())
    const [finishDate, setFinishDate] = useState(new Date())
    const [startDateEdit, setStartDateEdit] = useState(new Date(0))
    const [finishDateEdit, setFinishDateEdit] = useState(new Date(0))

    const [isCreating, setCreating] = useState<boolean>(false)
    const [isEdit, setIsEdit] = useState<boolean>(false)
    const [trainingEdit, setTrainingEdit] = useState<ITraining>({} as ITraining)

    const coaches = useSelector<IRootState>(state => state.coaches.coaches) as Array<ICoach>
    const disciplines = useSelector<IRootState>(state => state.disciplines.disciplines) as Array<IDiscipline>

    const isLoading = useSelector<IRootState>(state => state.trainings.isLoading) as boolean
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getTrainingsArray())
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    const openAdd = () => {
        setCreating(true)
        lockBodyElement()
    }

    const closeModals = () => {
        setCreating(false)
        setIsEdit(false)
        unlockBodyElement()
    }

    const addTrainingSubmitHandler = (e: React.FormEvent) => {
        e.preventDefault()
        const form = e.target as HTMLFormElement
        const elems: IElementTrainingType = form.elements as any
        const time = {
            startTime: startDate.getTime(),
            finishTime: finishDate.getTime()
        }
        
        dispatch(addTraining(elems, time))
    
        form.reset()
        closeModals()
    }

    const editFormHandler = (e: React.FormEvent) => {
        e.preventDefault()
        const form = e.target as HTMLFormElement
        const elems: IElementTrainingType = form.elements as any
        const time = {
            startTime: startDateEdit.getTime(),
            finishTime: finishDateEdit.getTime()
        }
        dispatch(editTraining(elems, trainingEdit, time))
    
        form.reset()
        closeModals()
        setTrainingEdit({} as ITraining)
    }

    const openEditTraining = (training: ITraining) => {
        setTrainingEdit(training)       
        setStartDateEdit(new Date(training.start))
        setFinishDateEdit(new Date(training.finish))
        setIsEdit(true)
        lockBodyElement()
    }

    return (
        <section className="training">
            {
                isLoading
                    ?   <div className="training-overlay">
                            <Loader />
                        </div>

                    :   <div className="container training-container">
                            <AddBtn title="Добавить тренеровку" onClick={openAdd}/>
                            <TrainingBody clickEditHandler={openEditTraining}/>
                        </div>             
            }
                
            <ModalAdd
                show={isCreating}
                title="Создание тренировки"
                onClose={closeModals}
            >
                <form className="add__form" onSubmit={addTrainingSubmitHandler}>
                    <div className="add__form-field">
                        <label htmlFor="add-title">Название тренировки</label>
                        <input type="text" placeholder="Введите название" name="title" id="add-title" required/>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="">Выберите время начала тренировки</label>
                        <DatePicker 
                            selected={startDate}
                            onChange={(date: Date) => setStartDate(date)}
                            locale={ru}
                            timeInputLabel="Time:"
                            dateFormat="dd/MM/yyyy HH:mm"
                            showTimeInput
                    />
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="">Выберите время окончания тренировки</label>
                        <DatePicker 
                            selected={finishDate}
                            onChange={(date: Date) => setFinishDate(date)}
                            locale={ru}
                            timeInputLabel="Time:"
                            dateFormat="dd/MM/yyyy HH:mm"
                            showTimeInput
                    />
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="add-description">Описание тренировки</label>
                        <input type="text" placeholder="Введите описание" name="description" id="add-description"/>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="add-discipline">Программа</label>
                        <select name="discipline" id="add-discipline">
                            {
                                disciplines.map(discipline => {
                                    return (
                                        <option value={`${discipline.id}`} key={discipline.id}>{discipline.title}</option>
                                    )
                                })
                            }
                        </select>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="add-coach">Тренер</label>
                        <select name="coach" id="add-coach">
                            {
                                coaches.map(coach => {
                                    return (
                                        <option value={`${coach.id}`} key={coach.id}>{coach.fullName}</option>
                                    )
                                })
                            }
                        </select>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="">Тренеровка завершена?</label>
                        <div className="add__form-radio">
                            <input type="radio" name="completed" value="true"/> Да
                            <input type="radio" name="completed" value="false" defaultChecked={true}/> Нет
                        </div>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="">Тренеровка удалена?</label>
                        <div className="add__form-radio">
                            <input type="radio" name="deleted" value="true"/> Да
                            <input type="radio" name="deleted" value="false" defaultChecked={true}/> Нет
                        </div>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="add-duration">Длительность тренировки, мин.</label>
                        <input type="number" placeholder="Введите длительность" name="duration" id="add-duration" defaultValue={0}/>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="">Free?</label>
                        <div className="add__form-radio">
                            <input type="radio" name="free" value="true"/> Да
                            <input type="radio" name="free" value="false" defaultChecked={true}/> Нет
                        </div>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="add-places">Количество мест</label>
                        <input type="number" placeholder="Введите количество мест" name="placesLimit" id="add-places" defaultValue={0}/>
                    </div>

                    <input type="submit" value="Добавить тренеровку" className="add__submit-btn"/>
                </form>
            </ModalAdd>


            <ModalAdd
                show={isEdit}
                title="Редактирование тренировки"
                onClose={closeModals}
            >
                <form className="add__form" onSubmit={editFormHandler}>
                    <div className="add__form-field">
                        <label htmlFor="add-title">Название тренировки</label>
                        <input type="text" placeholder="Введите название" name="title" id="add-title" required defaultValue={trainingEdit.title}/>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="">Выберите время начала тренировки</label>
                        <DatePicker 
                            selected={startDateEdit}
                            onChange={(date: Date) => setStartDateEdit(date)}
                            locale={ru}
                            timeInputLabel="Time:"
                            dateFormat="dd/MM/yyyy HH:mm"
                            showTimeInput
                    />
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="">Выберите время окончания тренировки</label>
                        <DatePicker 
                            selected={finishDateEdit}
                            onChange={(date: Date) => setFinishDateEdit(date)}
                            locale={ru}
                            timeInputLabel="Time:"
                            dateFormat="dd/MM/yyyy HH:mm"
                            showTimeInput
                    />
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="add-description">Описание тренировки</label>
                        <input type="text" placeholder="Введите описание" name="description" id="add-description" defaultValue={trainingEdit.description}/>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="add-discipline">Программа</label>
                        <select name="discipline" id="add-discipline" defaultValue={trainingEdit.discipline ? trainingEdit.discipline.id : 0}>
                            {
                                disciplines.map(discipline => (
                                    <option value={`${discipline.id}`} key={discipline.id}>
                                        {discipline.title}
                                    </option>
                                ))
                            }
                        </select>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="add-coach">Тренер</label>
                        <select name="coach" id="add-coach" defaultValue={trainingEdit.coach ? trainingEdit.coach.id : 0}>
                            {
                                coaches.map(coach => (
                                    <option value={`${coach.id}`} key={coach.id}>
                                        {coach.fullName}
                                    </option>
                                ))
                            }
                        </select>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="">Тренеровка завершена?</label>
                        <div className="add__form-radio">
                            <input type="radio" name="completed" value="true" defaultChecked={trainingEdit.completed === true}/> Да
                            <input type="radio" name="completed" value="false" defaultChecked={trainingEdit.completed === false}/> Нет
                        </div>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="">Тренеровка удалена?</label>
                        <div className="add__form-radio">
                            <input type="radio" name="deleted" value="true" defaultChecked={trainingEdit.deleted === true}/> Да
                            <input type="radio" name="deleted" value="false" defaultChecked={trainingEdit.deleted === false}/> Нет
                        </div>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="add-duration">Длительность тренировки, мин.</label>
                        <input type="number" placeholder="Введите длительность" name="duration" id="add-duration" defaultValue={trainingEdit.duration}/>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="">Free?</label>
                        <div className="add__form-radio">
                            <input type="radio" name="free" value="true" defaultChecked={trainingEdit.free === true}/> Да
                            <input type="radio" name="free" value="false" defaultChecked={trainingEdit.free === false}/> Нет
                        </div>
                    </div>

                    <div className="add__form-field">
                        <label htmlFor="add-places">Количество мест</label>
                        <input type="number" placeholder="Введите количество мест" name="placesLimit" id="add-places" defaultValue={trainingEdit.placesLimit}/>
                    </div>

                    <input type="submit" value="Изменить тренеровку" className="add__submit-btn"/>
                </form>
            </ModalAdd>
        </section>
    )
}

export default Training