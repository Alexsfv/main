import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css'
import MainLoader from './Components/MainLoader/MainLoader';
import Auth from './Pages/Auth/Auth';
import MainAdmin from './Pages/MainAdmin/MainAdmin';
import { IRootState } from './store/reducers/rootReducer';
import { checkStatusUser } from './store/actions/AppActions';
import Training from './Pages/Training/Training';
import Disciplines from './Pages/Disciplines/Disciplines';
import Coaches from './Pages/Coaches/Coaches';
import Directions from './Pages/Directions/Directions';
import NavBar from './Components/NavBar/NavBar';
import { updateCoaches } from './store/actions/CoachesActions';
import { updateDirections } from './store/actions/DirectionsActions';
import { updateDisciplines } from './store/actions/DisciplinesActions';
import { getTrainingsArray } from './store/actions/TrainingActions';

type OwnProps = {}

class App extends React.Component<OwnProps & StateProps & DispatchProps, {}> {

    componentDidMount() {       
        this.props.checkStatusUser()
    }

    componentDidUpdate() {
        if (this.props.isAdmin === true) {
            this.props.updateCoaches()
            this.props.updateDirections()
            this.props.updateDisciplines()
            this.props.getTrainingsArray()
        }
    }

    render() {
        if (this.props.isAdmin === null) {
            /// Если ответ с сервера о статусе админа еще не пришел
            return (
                <MainLoader />
            )
        }

        if (this.props.isAdmin === true) {
            return (
                <>
                    <NavBar></NavBar>
                    
                    <Switch>
                    <Route path="/admin" exact component={MainAdmin}/>
                    <Route path="/admin/training" component={Training} />
                    <Route path="/admin/disciplines" component={Disciplines} />
                    <Route path="/admin/coaches" component={Coaches} />
                    <Route path="/admin/directions" component={Directions} />

                    <Redirect to="/admin"/>
                </Switch>
                </>
            )
        }

        if (this.props.isAdmin === false) {
            return (
                <Switch>
                    <Route path="/auth" component={Auth}/>
                    <Redirect to="/auth"/>
                </Switch>
            )
        }

    }
}


type StateProps = {
    isAdmin: boolean | null
}
function mapStateToProps(state: IRootState) {
    return {
        isAdmin: state.app.isAdmin,
    }
}


type DispatchProps = {
    checkStatusUser: () => void
    updateCoaches: () => void
    updateDirections: () => void
    updateDisciplines: () => void
    getTrainingsArray: () => void
}
const mapDispatch: DispatchProps = {
    checkStatusUser: () => checkStatusUser(),
    updateCoaches: () => updateCoaches(),
    updateDirections: () => updateDirections(),
    updateDisciplines: () => updateDisciplines(),
    getTrainingsArray: () => getTrainingsArray(),
}

export default connect<StateProps, DispatchProps, OwnProps, IRootState>(mapStateToProps, mapDispatch)(App)
