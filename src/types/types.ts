export type IGetToken = {
    access_token: string
    token_type: string
    refresh_token: string
    expires_in: number
    scope: string
    jti: string
}

export type IGetAdminStatus = {
    is_admin: boolean
}

export type IElementTrainingType = {
  title: HTMLInputElement
  description: HTMLInputElement
  store: HTMLInputElement
  discipline: HTMLInputElement
  coach: HTMLInputElement
  completed: HTMLInputElement
  duration: HTMLInputElement
  free: HTMLInputElement
  placesLimit: HTMLInputElement
  deleted: HTMLInputElement
}

export type ITimeTraining = {
  startTime: number,
  finishTime: number
}


export type ITrainingClients = {
  "id": number,
  "deleted": boolean,
  "userId": number,
  "surname": null,
  "firstName": string,
  "patronymic": null,
  "fullName": string,
  "defaultStoreId": number,
  "stores": Array<number>,
  "role": null,
  "phone": string,
  "email": string,
  "sex": null,
  "city": string
}

export type ITraining = {
  "id": number,
  "start": number,
  "finish": number,
  "title": string,
  "description": string,
  "category": null,
  "store": {
    "id": number
  },
  "discipline": {
    "id": number
  },
  "coach": {
    "id": number,
    "name"?: null | string,
    "deleted": boolean,
    "userId": null | number,
    "surname": string,
    "firstName": string,
    "patronymic": null,
    "fullName": string,
    "defaultStoreId": null | number,
    "stores": null,
    "role": null | string,
    "phone": null | string,
    "email": null | string,
    "sex": null | string,
    "description": null | string
  },
  "clients": Array<ITrainingClients | null>,
  "completed": boolean,
  "deleted": boolean,
  "duration": number,
  "eventCalendarPlanning": null,
  "debitedClientService": {
    "id": null
  },
  "free": boolean,
  "numberMinutesBeforeClosing": number,
  "placesLimit": number,
  "canceled": boolean,
  "confirmed": boolean,
  "clientConfirmed": boolean
}

export type ICoach = {
  "id": number,
  "name": null | string,
  "deleted": boolean,
  "userId": null | number,
  "surname": string,
  "firstName": string,
  "patronymic": null,
  "fullName": string,
  "defaultStoreId": null | number,
  "stores": null,
  "role": null | string,
  "phone": null | string,
  "email": null | string,
  "sex": null | string,
  "description": null | string
}

export type IElementCoachType = {
  email: HTMLInputElement
  firstName: HTMLInputElement
  secondName: HTMLInputElement
  phone: HTMLInputElement
  sex: HTMLInputElement
  description: HTMLInputElement
}

export type IDiscipline = {
  id: number,
  title: string,
  includes: Array<number>
}

export type IElementDisciplineType = {
  title: HTMLInputElement
}

export type IDirection = {
  id: number,
  title: string,
  includes: Array<number>
}

export type IElementDirectionType = {
  title: HTMLInputElement
  [key: string]: HTMLInputElement
}